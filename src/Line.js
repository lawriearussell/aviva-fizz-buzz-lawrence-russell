import React, { Component } from 'react';
import './Line.css';

class Line extends React.Component {

    getFizzBang = () => {
        var returnValue = null;
        if (this.props.value % 5 === 0) {
            if (this.props.value % 3 === 0) {
                returnValue = <div><span className="blue">{this.getFizzWizz()}</span> <span className="green">{this.getBuzzWuzz()}</span></div>;
            } else {
                returnValue = <div><span className="green">{this.getBuzzWuzz()}</span></div>;
            }
        } else if (this.props.value % 3 === 0) {
            returnValue = <div><span className="blue">{this.getFizzWizz()}</span></div>;
        } else {
            returnValue = <div>{this.props.value}</div>;
        }
        return returnValue;
    };

    getFizzWizz() {
        return this.props.dayOfWeek !== 2 ? 'fizz' : 'wizz';
    }

    getBuzzWuzz() {
        return this.props.dayOfWeek !== 2 ? 'buzz' : 'wuzz';
    }

    render() {
        return (
                this.getFizzBang()
        );
    }
}

export default Line;

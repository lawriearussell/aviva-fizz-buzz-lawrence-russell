import React, { Component } from 'react';
import './App.css';
import Line from './Line';

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            inputValue: null,
            maxValue: 0
        }

    }

    getLines = (linesToShow) => {
        var lines = [];
        for(var i = 1; i <= linesToShow; i++) {
            lines.push(<Line key={i} value={i} dayOfWeek={this.getDate()} />);
        }
        return lines;
    };

    handleClick() {
        console.log(this.state.maxValue);
        this.setState((state) => ({maxValue: state.inputValue}));
    }

    handleInput(evt) {
        let userValue = this.clampNumber(evt.target.value, 1, 1000);
        this.setState({inputValue: userValue});
    }

    clampNumber(number, min, max) {
        return (number < min ? min : number > max ? max : number);
    }

    getDate() {
        return new Date().getDay();
    }

    render() {
        return (
                <div className="App">
                    <input value={this.state.userValue} onChange={evt => this.handleInput(evt)}/>
                    <button onClick={() => this.handleClick()}>Fizz Buzz</button>
                    {this.getLines(this.state.maxValue)}
                </div>
        );
    }
}



export default App;

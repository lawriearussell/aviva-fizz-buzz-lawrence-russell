import React from 'react';
import Line from './Line';
import ReactTestUtils from 'react-dom/test-utils';


describe('<Line>', () => {

    it('getFizzWizz to return Fizz not on Wednesdays', () => {
        let line = ReactTestUtils.renderIntoDocument(<Line value={3} dayOfWeek={1} />);
        expect(line.getFizzWizz()).toEqual('fizz');
    });

    it('getFizzWizz to return Wizz on Wednesdays', () => {
        let line = ReactTestUtils.renderIntoDocument(<Line value={3} dayOfWeek={2} />);
        expect(line.getFizzWizz()).toEqual('wizz');
    });

    it('getBuzzWuzz to return buzz not on Wednesdays', () => {
        let line = ReactTestUtils.renderIntoDocument(<Line value={3} dayOfWeek={1} />);
        expect(line.getBuzzWuzz()).toEqual('buzz');
    });

    it('getBuzzWuzz to return Wizz on Wednesdays', () => {
        let line = ReactTestUtils.renderIntoDocument(<Line value={3} dayOfWeek={2} />);
        expect(line.getBuzzWuzz()).toEqual('wuzz');
    });


    describe('On a Wednesday', () => {
        it('multiple of 3 returns \'fizz\'', () => {
            let line = ReactTestUtils.renderIntoDocument(<Line value={3} dayOfWeek={2} />);
            expect(line.getFizzBang()).toEqual(<div><span className="blue">wizz</span></div>);
        });

        it('multiple of 5 returns \'buzz\'', () => {
            let line = ReactTestUtils.renderIntoDocument(<Line value={5} dayOfWeek={2} />);
            expect(line.getFizzBang()).toEqual(<div><span className="green">wuzz</span></div>);
        });

        it('multiple of 3 and 5 returns \'fizz buzz\'', () => {
            let line = ReactTestUtils.renderIntoDocument(<Line value={15} dayOfWeek={2} />);
            expect(line.getFizzBang()).toEqual(<div><span className="blue">wizz</span> <span className="green">wuzz</span></div>);
        });

        it('not multiple of 3 or 5 returns a normal number', () => {
            let line = ReactTestUtils.renderIntoDocument(<Line value={11} dayOfWeek={2} />);
            expect(line.getFizzBang()).toEqual(<div>{11}</div>);
        });
    });

    describe('Not on a Wednesday', () => {
        it('multiple of 3 returns \'fizz\'', () => {
            let line = ReactTestUtils.renderIntoDocument(<Line value={3} dayOfWeek={1} />);
            expect(line.getFizzBang()).toEqual(<div><span className="blue">fizz</span></div>);
        });

        it('multiple of 5 returns \'buzz\'', () => {
            let line = ReactTestUtils.renderIntoDocument(<Line value={5} dayOfWeek={3} />);
            expect(line.getFizzBang()).toEqual(<div><span className="green">buzz</span></div>);
        });

        it('multiple of 3 and 5 returns \'fizz buzz\'', () => {
            let line = ReactTestUtils.renderIntoDocument(<Line value={15} dayOfWeek={5} />);
            expect(line.getFizzBang()).toEqual(<div><span className="blue">fizz</span> <span className="green">buzz</span></div>);
        });

        it('not multiple of 3 or 5 returns a normal number', () => {
            let line = ReactTestUtils.renderIntoDocument(<Line value={11} dayOfWeek={0} />);
            expect(line.getFizzBang()).toEqual(<div>{11}</div>);
        });
    });

});

import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import ReactTestUtils from "react-dom/test-utils";

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('clamps numbers correctly', () => {
    let app = ReactTestUtils.renderIntoDocument(<App />);
    expect(app.clampNumber(5, 10, 20)).toEqual(10);
    expect(app.clampNumber(15, 10, 20)).toEqual(15);
    expect(app.clampNumber(20, 10, 20)).toEqual(20);
});

it('clamp user input', () => {
    let app = ReactTestUtils.renderIntoDocument(<App />);

    app.handleInput({target: {value: -10}});
    expect(app.state.inputValue).toEqual(1);

    app.handleInput({target: {value: 2000}});
    expect(app.state.inputValue).toEqual(1000);

    app.handleInput({target: {value: 20}});
    expect(app.state.inputValue).toEqual(20);
});

